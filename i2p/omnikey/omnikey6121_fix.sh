#!/bin/sh

# This script is a fix for Omnikey 6121 Smart Card Readers, with productID 6632
# It will add the necessary configuration lines into udev rules and CCID config
# This will make sure that USB hotplug is working and the CCID driver will load

# Must be run as root!

RULES_FILE="/etc/udev/rules.d/z98_omnikey.rules"
PLIST_FILE="/usr/lib64/pcsc/drivers/ifd-ccid.bundle/Contents/Info.plist"

RULES_LINE_NORMAL="SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"076B\", ATTRS{idProduct}==\"6632\", ACTION==\"add\", RUN+=\"ok_pcscd_hotplug.sh\""
RULES_LINE_NEW="SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"076B\", ATTRS{idProduct}==\"6632\", ACTION==\"add\", RUN+=\"pcscd --hotplug\""

PLIST_VID_LINE="\t\t<string>0x076B</string>"
PLIST_PID_LINE="\t\t<string>0x6632</string>"
PLIST_FN_LINE="\t\t<string>OMNIKEY 6121 Smart Card Reader</string>"

# If Ominkey rules file exist ammend it. 
# If not, create one.

if [ -f $RULES_FILE ]; then
  if [ "$(grep -c ATTRS{idProduct}==\"6632\", $RULES_FILE)" = "0" ]; then 
     echo $RULES_LINE_NORMAL >> $RULES_FILE
  fi
else
  touch $RULES_FILE
  chmod 644 $RULES_FILE
  echo $RULES_LINE_NEW > $RULES_FILE
fi

# Ammend Info.plist if it does not have reference to 6632
# It must be inserted in 3 places

if [ -f $PLIST_FILE ]; then
  if [ "$(grep -c \<string\>0x6632\< $PLIST_FILE)" = "0" ]; then
    sed -i.bkp "
      \@<key>ifdVendorID</key>@ {
        N
        s@<array>@<array>\n$PLIST_VID_LINE@
      }
      \@<key>ifdProductID</key>@ {
        N
        s@<array>@<array>\n$PLIST_PID_LINE@
      }
      \@<key>ifdFriendlyName</key>@ {
        N
        s@<array>@<array>\n$PLIST_FN_LINE@
      }
    " $PLIST_FILE
  fi
else
  echo "CCID driver not installed. Install driver and re-run this script."
  exit 1
fi

exit 0
