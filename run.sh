#!/bin/bash

hash mkisofs 2>/dev/null || { echo "mkisofs is missing.  Run sudo yum install genisoimage."; exit 1; }

set -ex

shopt -s dotglob # include hidden files

#sha256sum -c SHA256SUMS

mkdir iso-tmp
sudo mount -t iso9660 -o loop rhel-server-7.1-x86_64-dvd.iso iso-tmp

mkdir iso
cp -a iso-tmp/* iso/
chmod -R +w iso/

sudo umount iso-tmp
rmdir iso-tmp

cp isolinux.cfg iso/isolinux/
cp -a ks-x86_64.cfg cc-packages/ i2p/ iso/

mkisofs -J -T -V "RHEL-7.1 Server.x86_64" \
 -b isolinux/isolinux.bin -c isolinux/boot.cat \
 -no-emul-boot -boot-load-size 4 -boot-info-table -R -m TRANS.TBL \
 -o rhel-server-7.1-cc-x86_64-dvd.iso -graft-points -quiet iso/

#mkisofs -r -T -J -V "RHEL-7.1 Server.x86_64" \
#  -b isolinux/isolinux.bin -c isolinux/boot.cat \
#  -no-emul-boot -boot-load-size 4 -boot-info-table \
#  -o rhel-server-7.1-cc-x86_64-dvd2.iso -quiet iso/

# mkisofs -U -r -v -T -J -joliet-long -V "RHEL-7.1 Server.x86_64" -volset "RHEL-7.1 Server.x86_64" -A "RHEL-7.1 Server.x86_64" \
#   -b isolinux/isolinux.bin -c isolinux/boot.cat \
#   -no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e images/efiboot.img \
#   -o rhel-server-7.1-cc-x86_64-dvd3.iso -quiet iso/

rm -rf iso/
