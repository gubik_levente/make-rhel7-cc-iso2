#
# Modified configuration for CC Base / MLS mode compliance
#
# su alias, added for EAL4+ configuration
alias su "echo Always use \'/bin/su -\' \(see \'Evaluated Configuration Guide\'\)"

#
# The following environment variable disabled the use of:
#
# a) either AES-NI Intel processor instruction set (this is required 
#    by BSI as the AES-NI instruction set was not subject to evaluation);
#
# setenv OPENSSL_ia32cap=~0x200000000000000
#
# b) or RDRAND instruction support;
#
# setenv OPENSSL_ia32cap=~0x4000000000000000
#
# c) or both a) and b).
#
setenv OPENSSL_ia32cap=~0x4000000000000000
readonly OPENSSL_ia32cap

# ssh / ssh-keygen use of /dev/random as seed source
# You MUST enable the following lines to be compliant with the
# requirements of the evaluated configuration. For a description of this
# configuration and its implication, see /etc/sysconfig/sshd. Use the
# 'readonly' setting to ensure that all users are required to use this
# variable.
setenv SSH_USE_STRONG_RNG=12
readonly SSH_USE_STRONG_RNG

#
# Setting the umask value to a DISA STIG compliant value for normal users
# This setting overrides the setting in /etc/profile
umask 027
